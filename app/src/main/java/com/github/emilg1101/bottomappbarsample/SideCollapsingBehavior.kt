package com.github.emilg1101.bottomappbarsample

import android.animation.*
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.AnimatedImageDrawable
import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.ViewPropertyAnimator
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import com.google.android.material.animation.AnimationUtils
import com.google.android.material.appbar.AppBarLayout

@SuppressLint("NewApi")
class SideCollapsingBehavior(context: Context, attrs: AttributeSet?) : CoordinatorLayout.Behavior<AppBarLayout>(context, attrs) {

    private var targetView: View? = null
    private var toolbar: Toolbar? = null

    private var collapsedAppBarWidth = 0
    private var appBarHeight = 0
    private var appBarWidth = 0
    private var currentState = STATE_SCROLLED_RIGHT
    private var defaultElevation = context.resources.getDimension(R.dimen.design_appbar_elevation)
    private var currentAnimator: ViewPropertyAnimator? = null

    override fun onStartNestedScroll(coordinatorLayout: CoordinatorLayout, child: AppBarLayout, directTargetChild: View, target: View, axes: Int, type: Int): Boolean {
        return axes == ViewCompat.SCROLL_AXIS_VERTICAL
    }

    override fun onLayoutChild(parent: CoordinatorLayout, child: AppBarLayout, layoutDirection: Int): Boolean {
        val paramsCompat = child.layoutParams as ViewGroup.MarginLayoutParams
        appBarHeight = child.getChildAt(0).measuredHeight + paramsCompat.topMargin
        //parentHeight = parent.measuredHeight
        if (targetView == null) {
            toolbar = child.getChildAt(0) as Toolbar?
            appBarWidth = child.getChildAt(0).measuredWidth + paramsCompat.rightMargin + paramsCompat.leftMargin
            targetView = parent.getChildAt(0)
            targetView?.translationY = appBarHeight.toFloat()
        }
        return super.onLayoutChild(parent, child, layoutDirection)
    }

    override fun onNestedScroll(coordinatorLayout: CoordinatorLayout, child: AppBarLayout, target: View, dxConsumed: Int, dyConsumed: Int, dxUnconsumed: Int, dyUnconsumed: Int, type: Int) {
        if (currentState == STATE_SCROLLED_RIGHT && dyConsumed > 20) {
            child.elevation = defaultElevation
            slideLeft(child)
            targetView?.let { it.animate().translationY(0f).duration = ENTER_ANIMATION_DURATION }
        } else if (currentState == STATE_SCROLLED_LEFT && dyConsumed == 0 && dyUnconsumed < -20) {
            slideRight(child)
            child.elevation = 0f
            targetView?.let { it.animate().translationY(appBarHeight.toFloat()).duration = EXIT_ANIMATION_DURATION }
        }
        Log.d("MYLOGS", "dyConsumed = $dyConsumed, dyUnconsumed = $dyUnconsumed, currentState = $currentState")
    }

    private fun slideRight(child: AppBarLayout) {
        toolbar?.let { toolbar ->
            currentAnimator?.let {
                it.cancel()
                child.clearAnimation()
                toolbar.clearAnimation()
            }
            currentState = STATE_SCROLLED_RIGHT
            currentAnimator = toolbar.getChildAt(0).animate().alpha(1f).setDuration(ENTER_ANIMATION_DURATION).setInterpolator(AnimationUtils.FAST_OUT_LINEAR_IN_INTERPOLATOR).setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    currentAnimator = null
                    toolbar.getChildAt(0).visibility = View.VISIBLE
                    toolbar.titleMarginStart = 12
                    toolbar.titleMarginEnd = 12
                }
            })
            child.startAnimation(ResizeAnimation(child, appBarWidth).apply {
                duration = ENTER_ANIMATION_DURATION
            })
        }
        Log.d("MYLOGS", "slideRight")
    }

    private fun slideLeft(child: AppBarLayout) {
        toolbar?.let { toolbar ->
            currentAnimator?.let {
                it.cancel()
                child.clearAnimation()
                toolbar.clearAnimation()
            }
            currentState = STATE_SCROLLED_LEFT
            val newWidth = calcCollapsedAppBarWidth(child)

            currentAnimator = toolbar.getChildAt(0).animate().alpha(0f).setDuration(EXIT_ANIMATION_DURATION).setInterpolator(AnimationUtils.FAST_OUT_LINEAR_IN_INTERPOLATOR).setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    currentAnimator = null
                    toolbar.getChildAt(0).visibility = View.GONE
                    toolbar.titleMarginStart = 0
                    toolbar.titleMarginEnd = 0
                }
            })
            child.startAnimation(ResizeAnimation(child, newWidth).apply {
                duration = EXIT_ANIMATION_DURATION
            })

            Log.d("MYLOGS", "slideLeft")
        }
    }

    private fun calcCollapsedAppBarWidth(child: AppBarLayout): Int {
        if (collapsedAppBarWidth == 0) {
            val paramsCompat = child.layoutParams as ViewGroup.MarginLayoutParams
            val toolbar = child.getChildAt(0) as Toolbar
            collapsedAppBarWidth = toolbar.getChildAt(1).measuredWidth + toolbar.getChildAt(2).measuredWidth + paramsCompat.leftMargin + paramsCompat.rightMargin
        }
        return collapsedAppBarWidth
    }

    private fun animateChildTo(child: AppBarLayout, targetY: Float, duration: Long, interpolator: TimeInterpolator, startDelay: Long, alpha: Float) {
        currentAnimator = child
                .animate()
                .alpha(alpha)
                .setStartDelay(startDelay)
                .translationY(targetY)
                .setInterpolator(interpolator)
                .setDuration(duration)
                .setListener(
                        object : AnimatorListenerAdapter() {
                            override fun onAnimationEnd(animation: Animator) {
                                currentAnimator = null
                            }
                        })
    }

    companion object {
        private const val STATE_SCROLLED_LEFT = 1
        private const val STATE_SCROLLED_RIGHT = 2
        private const val ENTER_ANIMATION_DURATION = 150L
        private const val EXIT_ANIMATION_DURATION = 175L
    }
}