package com.github.emilg1101.bottomappbarsample

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.TimeInterpolator
import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.ViewPropertyAnimator
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.ViewCompat
import com.google.android.material.animation.AnimationUtils
import com.google.android.material.appbar.AppBarLayout

@SuppressLint("NewApi")
class MyBehavior(context: Context, attrs: AttributeSet?) : CoordinatorLayout.Behavior<AppBarLayout>(context, attrs) {

    private var currentAnimator: ViewPropertyAnimator? = null

    private var currentState = STATE_SCROLLED_UP

    private var height = 0
    private var parentHeight = 0
    private var defaultElevation = context.resources.getDimension(R.dimen.design_appbar_elevation)
    private var targetView: View? = null

    override fun onLayoutChild(parent: CoordinatorLayout, child: AppBarLayout, layoutDirection: Int): Boolean {
        val paramsCompat = child.layoutParams as ViewGroup.MarginLayoutParams
        height = child.getChildAt(0).measuredHeight + paramsCompat.topMargin
        parentHeight = parent.measuredHeight
        targetView = parent.getChildAt(0)
        targetView?.translationY = height.toFloat()
        return super.onLayoutChild(parent, child, layoutDirection)
    }

    override fun onStartNestedScroll(coordinatorLayout: CoordinatorLayout, child: AppBarLayout, directTargetChild: View, target: View, axes: Int, type: Int): Boolean {
        return axes == ViewCompat.SCROLL_AXIS_VERTICAL
    }

    override fun onNestedScroll(coordinatorLayout: CoordinatorLayout, child: AppBarLayout, target: View, dxConsumed: Int, dyConsumed: Int, dxUnconsumed: Int, dyUnconsumed: Int, type: Int) {
        if (currentState != STATE_SCROLLED_DOWN && dyConsumed in 1..10) {
            child.elevation = defaultElevation
        } else if (currentState != STATE_SCROLLED_DOWN && dyConsumed > 10) {
            child.elevation = defaultElevation
            slideUp(child)
            targetView?.let { it.animate().translationY(0f).duration = EXIT_ANIMATION_DURATION }
        } else if (currentState != STATE_SCROLLED_UP && dyConsumed < -10) {
            slideDown(child)
            child.elevation = defaultElevation
            targetView?.let { it.animate().translationY(height.toFloat()).duration = ENTER_ANIMATION_DURATION }
        } else if (currentState == STATE_SCROLLED_DOWN && currentAnimator == null) {
            child.elevation = 0f
        } else if (dyConsumed == 0) {
            child.elevation = 0f
        }
    }

    private fun slideUp(view: AppBarLayout) {
        currentAnimator?.let {
            it.cancel()
            view.clearAnimation()
        }
        currentState = STATE_SCROLLED_DOWN
        animateChildTo(
                view, 0f - height, EXIT_ANIMATION_DURATION, AnimationUtils.FAST_OUT_LINEAR_IN_INTERPOLATOR, 250, 0f)
        Log.d("MYLOGS", "slideUp")
    }

    private fun slideDown(view: AppBarLayout) {
        currentAnimator?.let {
            it.cancel()
            view.clearAnimation()
        }
        currentState = STATE_SCROLLED_UP
        animateChildTo(
                view, 0f, ENTER_ANIMATION_DURATION, AnimationUtils.LINEAR_OUT_SLOW_IN_INTERPOLATOR, 0, 1f)
        Log.d("MYLOGS", "slideDown")
    }

    private fun animateChildTo(child: AppBarLayout, targetY: Float, duration: Long, interpolator: TimeInterpolator, startDelay: Long, alpha: Float) {
        currentAnimator = child
                .animate()
                .alpha(alpha)
                .setStartDelay(startDelay)
                .translationY(targetY)
                .setInterpolator(interpolator)
                .setDuration(duration)
                .setListener(
                        object : AnimatorListenerAdapter() {
                            override fun onAnimationEnd(animation: Animator) {
                                currentAnimator = null
                            }
                        })
    }

    companion object {
        private const val STATE_SCROLLED_DOWN = 1
        private const val STATE_SCROLLED_UP = 2
        private const val ENTER_ANIMATION_DURATION = 225L
        private const val EXIT_ANIMATION_DURATION = 175L
    }
}
